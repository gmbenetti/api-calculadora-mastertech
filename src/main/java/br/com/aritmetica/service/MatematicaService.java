package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n: entradaDTO.getNumeros()) {
            numero += n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }
    public RespostaDTO multiplica(EntradaDTO entradaDTO){
        int numero = 1;
        for (int n: entradaDTO.getNumeros()) {
            numero *= n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }
    public  RespostaDTO subtrai(EntradaDTO entradaDTO){
        int resultado = 0;
        int numeroEntrada1 = 0;
        int numeroEntrada2 = 0;

        numeroEntrada1 = entradaDTO.getNumeros().get(0);
        for(int i=1; i < entradaDTO.getNumeros().size(); i++){
            numeroEntrada2 += entradaDTO.getNumeros().get(i);
        }

        resultado = numeroEntrada1 - numeroEntrada2;
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);

        return resposta;
    }

    public RespostaDTO divide(EntradaDTO entradaDTO){
        int resultado = 0;
        int numeroEntrada1 = 0;
        int numeroEntrada2 = 0;

        numeroEntrada1 = entradaDTO.getNumeros().get(0);
        numeroEntrada2 = entradaDTO.getNumeros().get(1);

        if(numeroEntrada1 >= numeroEntrada2 ){
            resultado = numeroEntrada1 / numeroEntrada2;
        }else{
            resultado = numeroEntrada2 / numeroEntrada1;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }

}
