package br.com.aritmetica.DTOs;

import java.util.List;

public class EntradaDTO {

    public EntradaDTO(){}

    private List<Integer> numeros;

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
