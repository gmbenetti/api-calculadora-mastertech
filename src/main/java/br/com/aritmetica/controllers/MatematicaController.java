package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;
    //private MatematicaService matematicaService = new MatematicaService(); //sem o Autowired, tem que instanciar o objeto

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){
        validaQuantidadeNumeros(entradaDTO);
        validaNumerosNegativos(entradaDTO);

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/multiplica")
    public RespostaDTO multiplica(@RequestBody EntradaDTO entradaDTO){
        validaQuantidadeNumeros(entradaDTO);
        validaNumerosNegativos(entradaDTO);

        RespostaDTO resposta = matematicaService.multiplica(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtrai")
    public RespostaDTO subtrai(@RequestBody EntradaDTO entradaDTO){
        validaQuantidadeNumeros(entradaDTO);
        validaNumerosNegativos(entradaDTO);

        RespostaDTO resposta = matematicaService.subtrai(entradaDTO);
        return resposta;
    }

    @PutMapping("/divide")
    public RespostaDTO divide(@RequestBody EntradaDTO entradaDTO){
        validaQuantidadeNumerosDivisao(entradaDTO);
        validaNumerosNegativos(entradaDTO);
        validaZeros(entradaDTO);

        RespostaDTO resposta = matematicaService.divide(entradaDTO);
        return resposta;
    }

    private void validaZeros(EntradaDTO entradaDTO){
        for (int n: entradaDTO.getNumeros()) {
            if(n == 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Zeros não são permitidos para essa operação");
            }
        }
    }

    private void validaNumerosNegativos(EntradaDTO entradaDTO){
        for (int n: entradaDTO.getNumeros()) {
            if(n < 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Números negativos não são permitidos nas operações");
            }
        }
    }

    private void validaQuantidadeNumerosDivisao(EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() != 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Para divisão, envie 2 números");
        }
    }

    private void validaQuantidadeNumeros(EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos dois números");
        }
    }

}
